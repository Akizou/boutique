/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.util.Objects;

public class Client {
    private  String carteVisa;

    public void setCarteVisa(String carteVisa) {
        this.carteVisa = carteVisa;
    }

    public String getCarteVisa() {
        return carteVisa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(getCarteVisa(), client.getCarteVisa());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCarteVisa());
    }

    @Override
    public String toString() {
        return "Client{" +
                "carteVisa='" + carteVisa + '\'' +
                '}';
    }
}

    

