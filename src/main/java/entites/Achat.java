/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.time.LocalDate;
import java.util.ArrayList;

public class Achat {
    
     private long id;
    private double remise=0;
    private LocalDate dateAchat;
    private ArrayList<ProdiutAchete>prodiutAchetes;

    public double getRemiseTotale(){
        return 0;
    }

    public double getPrixTotal() {
        return 0;
    }

    public ArrayList<ProdiutAchete> getProdiutAchetes() {
        return prodiutAchetes;
    }

    public double getRemise() {
        return remise;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Achat)) return false;
        Achat achat = (Achat) o;
        return getId() == achat.getId() && Double.compare(achat.getRemise(), getRemise()) == 0 && Objects.equals(getDateAchat(), achat.getDateAchat()) && Objects.equals(getProdiutAchetes(), achat.getProdiutAchetes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRemise(), getDateAchat(), getProdiutAchetes());
    }

    public long getId() {
        return id;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProdiutAchetes(ArrayList<ProdiutAchete> prodiutAchetes) {
        this.prodiutAchetes = prodiutAchetes;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    @Override
    public String toString() {
        return "Achat{" +
                "id=" + id +
                ", remise=" + remise +
                ", dateAchat=" + dateAchat +
                ", prodiutAchetes=" + prodiutAchetes +
                '}';
    }
    
}
