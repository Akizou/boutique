/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;


/**
 *
 * @author pirichakiz
 */
public class Employe {
      private  String cnss;
    private LocalDate dateEmbauche;
    private ArrayList<Achat>achats;

    public void setAchats(ArrayList<Achat> achats) {
        this.achats = achats;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }
    public String getCnss(){
        return this.cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public ArrayList<Achat> getAchats() {
        return achats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employe)) return false;
        Employe employe = (Employe) o;
        return Objects.equals(getCnss(), employe.getCnss()) && Objects.equals(getDateEmbauche(), employe.getDateEmbauche()) && Objects.equals(getAchats(), employe.getAchats());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCnss(), getDateEmbauche(), getAchats());
    }

    @Override
    public String toString() {
        return "Employe{" +
                "cnss='" + cnss + '\'' +
                ", dateEmbauche=" + dateEmbauche +
                ", achats=" + achats +
                '}';
    }
}

    

