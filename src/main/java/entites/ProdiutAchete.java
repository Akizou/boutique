/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.util.Objects;

public class ProdiutAchete {
    
     private int  quantite=1;
    private double remise=0;


    public double getPrixTotal(){
        return 0;
    }

    public double getRemise() {
        return remise;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProdiutAchete)) return false;
        ProdiutAchete that = (ProdiutAchete) o;
        return getQuantite() == that.getQuantite() && Double.compare(that.getRemise(), getRemise()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuantite(), getRemise());
    }

    @Override
    public String toString() {
        return "ProdiutAchete{" +
                "quantite=" + quantite +
                ", remise=" + remise +
                '}';
    }
    
}
