/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.time.LocalDate;
import java.util.Objects;


public class Prodiut {
    
     private long id;
    private String libelle;
    private double prixUnitaire;
    private LocalDate datePeremption;


    public boolean estPerime() {
        return true;
    }

    public boolean estPerime(LocalDate ref) {
        return true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public double getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(double prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public LocalDate getDatePeremption() {
        return datePeremption;
    }

    public void setDatePeremption(LocalDate datePeremption) {
        this.datePeremption = datePeremption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Produit)) return false;
        Produit produit = (Produit) o;
        return getId() == produit.getId() && Double.compare(produit.getPrixUnitaire(), getPrixUnitaire()) == 0 && Objects.equals(getLibelle(), produit.getLibelle()) && Objects.equals(getDatePeremption(), produit.getDatePeremption());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLibelle(), getPrixUnitaire(), getDatePeremption());
    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", prixUnitaire=" + prixUnitaire +
                ", datePeremption=" + datePeremption +
                '}';
    }
    
}
