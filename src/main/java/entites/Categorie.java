/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.util.ArrayList;
import java.util.Objects;

public class Categorie {
    
     private long id;
    private String libelle;
    private  String description;
    private ArrayList<Produit> produits;

    public void setId(long id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setProduits(ArrayList<Produit> produits) {
        this.produits = produits;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public long getId() {
        return id;
    }

    public ArrayList<Produit> getProduits() {
        return produits;
    }

    public String getDescription() {
        return description;
    }

    public String getLibelle() {
        return libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Categorie)) return false;
        Categorie categorie = (Categorie) o;
        return getId() == categorie.getId() && Objects.equals(getLibelle(), categorie.getLibelle()) && Objects.equals(getDescription(), categorie.getDescription()) && Objects.equals(getProduits(), categorie.getProduits());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getLibelle(), getDescription(), getProduits());
    }

    @Override
    public String toString() {
        return "Categorie{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                ", description='" + description + '\'' +
                ", produits=" + produits +
                '}';
    }
    
}
