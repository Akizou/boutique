/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entites;

/**
 *
 * @author pirichakiz
 */
import java.time.LocalDate;
import java.util.Objects;

public class Personne {
    

public class Personne {
        protected long id;
        protected String nom;
        protected String prenom;
        protected LocalDate dateNaissance;

        public int getAge(){
            return 0;
        };

        public  int getAge(LocalDate ref){
            return 0;
        };

    }
   @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personne)) return false;
        Personne personne = (Personne) o;
        return id == personne.id && Objects.equals(nom, personne.nom) && Objects.equals(prenom, personne.prenom) && Objects.equals(dateNaissance, personne.dateNaissance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, dateNaissance);
    }

    @Override
    public String toString() {
        return "Personne{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", dateNaissance=" + dateNaissance +
                '}';
    }
    
}
